import { all } from 'redux-saga/effects';

import AuthSaga from './auth/login/saga';

//public
export default function* rootSaga() {
    yield all([
        //public
        AuthSaga()
    ])
}