import { combineReducers } from "redux";

import Login from './auth/login/reducer'

const rootReducer = combineReducers({
    // public
    Login
  });
  
  export default rootReducer;