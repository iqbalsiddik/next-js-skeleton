import { LOGIN_USER } from './actionTypes';

const initialState = {
    loading: false
}

const login = (state = initialState, action: any) => {
    switch (action.type) {
        case LOGIN_USER:
            state = {
                ...state,
                loading: true
            }
            break;
    }
    return state;
}

export default login;