import { LOGIN_USER, LOGIN_SUCCESS, LOGIN_ERROR } from './actionTypes';

export const loginUser = () => {
    return {
        type: LOGIN_USER,
    };
};

export const loginSuccess = (user: any) => {
    return {
        type: LOGIN_SUCCESS,
        payload: user
    };
};

export const loginError = (error: any) => {
    return {
        type: LOGIN_ERROR,
        payload: error
    };
};
