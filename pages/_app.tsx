import "tailwindcss/tailwind.css";
import type { AppProps } from "next/app";
import { Provider as ReduxProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ToastContainer } from 'react-toastify';

import { persiststor, store } from "../store";

function MyApp({ Component, pageProps }: AppProps) {

  return (
    <>
      <ReduxProvider store={store}>
        <PersistGate loading={ null } persistor={ persiststor }>
          <ToastContainer />
          <Component {...pageProps} />
        </PersistGate>
      </ReduxProvider>
    </>
  );
}
export default MyApp;
