/* eslint-disable require-yield */
/* eslint-disable no-undef */
import { takeEvery, fork, put, all, call } from 'redux-saga/effects';

import { loginError, loginSuccess } from './action';
import { LOGIN_USER } from './actionTypes';
import  request  from '../../../utils/request';


function* loginUser() {
    const requestURL: String = `${process.env.NEXT_PUBLIC_APP_URL}/cakes?page=1`;
    const headers: any = {
		'Content-Type': 'application/json',
	};
    
	const options: any = {
		method: 'GET',
		headers,
	};
    
    try {
        const response: any = yield call(request, requestURL, options);
        yield put(loginSuccess(response));
    } catch (error) {
        yield put(loginError(error));
    }
}

export function* watchUserLogin() {
    yield takeEvery(LOGIN_USER, loginUser);
}

function* authSaga() {
    yield all([
        fork(watchUserLogin),
    ]);
}

export default authSaga;